# Based on the file created for Arch Linux by:
# Tobias Powalowski <tpowa@archlinux.org>
# Ronald van Haren <ronald.archlinux.org>
# Keshav Padram <(the.ridikulus.rat) (aatt) (gemmaeiil) (ddoott) (ccoomm)>

# Maintainer: Philip Mueller <philm|manjaro|org>
# Maintainer: Guinux <nuxgui|gmail|com>
# Maintainer: Stefano Capitani <stefanoatmanjarodotorg>

_pkgver="2.03"
_GRUB_GIT_TAG="2.03"
_SNAPSHOT="8ada906031d9bd86547db82647f91cdf7db54fbf"
_SNAPSHOT_EXTRAS="f2a079441939eee7251bf141986cdd78946e1d20"

_UNIFONT_VER="10.0.06"

[[ "${CARCH}" == "x86_64" ]] && _EFI_ARCH="x86_64"
[[ "${CARCH}" == "i686" ]] && _EFI_ARCH="i386"

pkgname="grub-quiet"
_pkgname="grub"
pkgdesc="GNU GRand Unified Bootloader (Silent grub menu version )"
pkgver=2.03.0
pkgrel=13
url="https://www.gnu.org/software/grub/"
arch=('x86_64' 'i686')
license=('GPL3')
backup=('boot/grub/grub.cfg' 'etc/default/grub' 'etc/grub.d/40_custom')
install="${_pkgname}.install"
options=('!makeflags')

conflicts=('grub-common' 'grub-bios' "grub-efi-${_EFI_ARCH}" 'grub-legacy' 'grub')
provides=('grub-common' 'grub-bios' "grub-efi-${_EFI_ARCH}" 'grub')
replaces=('grub-dev')

makedepends=('git' 'rsync' 'xz' 'freetype2' 'ttf-dejavu' 'python' 'autogen'
             'texinfo' 'help2man' 'gettext' 'device-mapper' 'fuse')
depends=('sh' 'xz' 'gettext' 'device-mapper')
optdepends=('freetype2: For grub-mkfont usage'
            'fuse: For grub-mount usage'
            'dosfstools: For grub-mkrescue FAT FS and EFI support' 
            'efibootmgr: For grub-install EFI support'
            'libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue'
            'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
            'mtools: For grub-mkrescue FAT FS support')

source=(#"grub-${_pkgver}::git+git://git.sv.gnu.org/grub.git#tag=${_GRUB_GIT_TAG}"
        "grub-${_pkgver}-${pkgrel}.tar.gz::http://git.savannah.gnu.org/cgit/grub.git/snapshot/grub-${_SNAPSHOT}.tar.gz"
        "grub-extras-${_pkgver}-${pkgrel}.tar.gz::http://git.savannah.gnu.org/cgit/grub-extras.git/snapshot/grub-extras-${_SNAPSHOT_EXTRAS}.tar.gz"
        "http://ftp.gnu.org/gnu/unifont/unifont-${_UNIFONT_VER}/unifont-${_UNIFONT_VER}.bdf.gz"
        #"http://ftp.gnu.org/gnu/unifont/unifont-${_UNIFONT_VER}/unifont-${_UNIFONT_VER}.bdf.gz.sig"
        'grub-revert-6400613.patch'
        'grub-add-GRUB_COLOR_variables.patch'
        'grub-manjaro-modifications.patch'
        'grub-use-efivarfs.patch'
        'background.png'
        'grub.default'
        'grub.cfg'
        'update-grub'
	'00_header.in.patch'
	'30_os-prober.in.patch'
        'maybe_quiet.patch'
        https://github.com/rhboot/grub2/commit/479dbb9.patch
        https://github.com/rhboot/grub2/commit/9c8f258.patch
        https://github.com/rhboot/grub2/commit/894d3e2.patch
        https://github.com/rhboot/grub2/commit/fd6c8f6.patch
        https://github.com/rhboot/grub2/commit/6e9e312.patch
        https://github.com/rhboot/grub2/commit/88dc875.patch
        https://github.com/rhboot/grub2/commit/1424154.patch
        https://github.com/rhboot/grub2/commit/1a52178.patch
        https://github.com/rhboot/grub2/commit/ca163ee.patch
        https://github.com/rhboot/grub2/commit/96a3829.patch
        https://github.com/rhboot/grub2/commit/66f3c60.patch
        https://github.com/rhboot/grub2/commit/74fe525.patch
        https://github.com/rhboot/grub2/commit/150164a.patch
	"${_pkgname}.hook")

sha256sums=('83f559c61510760ea7d8484d9609948eedfc090ae2ea9d704936b81cde9ab582'
            '2844601914cea6b1231eca0104853a93c4d67a5209933a0766f1475953300646'
            '0d81571fc519573057b7641d26a31ead55cc0b02a931589fb346a3a534c3dcc1'
            '40401632b8d790976a80f3075fc9bfe8197b9b3b21080bbba517e7dd0784389a'
            'a5198267ceb04dceb6d2ea7800281a42b3f91fd02da55d2cc9ea20d47273ca29'
            'cf00c96aee37e0a73c1ab6ed6ccfe74fa2b2859f55cd315a4caa6c880ce7aeba'
            '20b2b6e7f501596b5cce6ffa05906980427f760c03d308d0e045cf2ecf47bb0e'
            '01264c247283b7bbdef65d7646541c022440ddaf54f8eaf5aeb3a02eb98b4dd8'
            '8f5e5e26607f5d7f17b92e49d71a308612a5ac69bd3db3c95b0a1a01b6cad121'
            '7fc95d49c0febe98a76e56b606a280565cb736580adecf163bc6b5aca8e7cbd8'
            '467b0101154076fee99d9574a5fb6b772a3923cc200a1f4ca08fe17be8d68111'
            '8675f35b82cc745aff95ba1097b8ef6e33be28125c7f484a14e90b29e809517d'
            '0745c7b3042fef0d9cf5d55cfcbcbed88fc859f9190757dfb68336eb32aaadf2'
            'd132a3d51c09d13179f84749127030e0eca370f76fbd76999de6a77960d1f7b1'
            'ee3adccd919870c89f87f835ba3ccd85d18c2cdd15e8e5488a9a68b5a0df19ad'
            '87fe6b9facf8584f6340cb10fe1cf22963c6c1a28a46c3708079dd8866a15c5d'
            '1dd5af80a254c3a1280c830feb1b90b0cbef08c81bf1d38d7ab4e650b1cd996e'
            'f7c259f7df9a07127e73029a90ffee0e8172c88e972324caa95659159cba8368'
            'e186bffae7d661b90d3eda6ce627d838cf543ae63112b383e0ce7e7c2349c76d'
            'aa124da42fa8e594941760065e615744dbf411f14feb73c29f4ed4b0ca5fd3e3'
            'bc5e21663f444d48a81d307a4ca0576f16e263eb6566297680dcb9bb529d14a6'
            '488d13363e692b3fcfacd9253904dde01147f1ecba9b0876a1598fe06e342546'
            '468c6edc9a3a7595d8d09fe1cd34e22f1375be61ce1003160b5d2c4c9c22182a'
            'f0bdcb062710555ef19272a243d2f058aef878fa331942428ac33943e1adadac'
            '8feb106c3d44dd257bd379efb7a7e4a003039748216a6a591838e5222fee652d'
            '4da4fead43a3006c8b40630c3f68c3a6a96203e476cd45f5cca2a7fe223bbeef'
            '800fb77051112316ec314a627a6c7db4857789b147203bbf5acb75b299c6390c'
            '1488d7f3924bd7385a222e3e9685cdb1ecb39f3d6f882da6b5907b898f5b8f08')

#pkgver() {
#	cd "${srcdir}/grub-${_pkgver}/"
#	echo "$(git describe --tags)" | sed -e 's|grub.||g' -e 's|-|\.|g'
#}

prepare() {
	mv "${srcdir}/grub-${_SNAPSHOT}/" "${srcdir}/grub-${_pkgver}/"
	mv "${srcdir}/grub-extras-${_SNAPSHOT_EXTRAS}/" "${srcdir}/grub-extras-${_pkgver}/"

	cd "${srcdir}/grub-${_pkgver}/"

	#https://github.com/calamares/calamares/issues/918
	msg "Revert commit 6400613"
	patch -Rp1 -i "${srcdir}/grub-revert-6400613.patch"
	echo

	# https://github.com/calamares/calamares/issues/918
	msg "Use efivarfs modules"
	patch -Np1 -i "${srcdir}/grub-use-efivarfs.patch"
	echo
	
	msg "Patch to enable GRUB_COLOR_* variables in grub-mkconfig"
	## Based on http://lists.gnu.org/archive/html/grub-devel/2012-02/msg00021.html
	patch -Np1 -i "${srcdir}/grub-add-GRUB_COLOR_variables.patch"
	echo

	msg "Patch to include Manjaro Linux Modifications"
	patch -Np1 -i "${srcdir}/grub-manjaro-modifications.patch"
	echo
	
	msg "Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme"
	sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i "${srcdir}/grub-${_pkgver}/configure.ac"
	
	# msg "autogen.sh requires python (2/3). since bzr is in makedepends, use python2 and no need to pull python3"
	# sed 's|python |python2 |g' -i "${srcdir}/grub-${_pkgver}/autogen.sh"
	
	msg "Pull in latest language files"
	./linguas.sh
	echo
	
	msg "Remove not working langs which need LC_ALL=C.UTF-8"
	sed -e 's#en@cyrillic en@greek##g' -i "${srcdir}/grub-${_pkgver}/po/LINGUAS"
	
	msg "Avoid problem with unifont during compile of grub, http://savannah.gnu.org/bugs/?40330 and https://bugs.archlinux.org/task/37847"
	cp "${srcdir}/unifont-${_UNIFONT_VER}.bdf" "${srcdir}/grub-${_pkgver}/unifont.bdf"

	# Use fedora patches to enable auto-hide boot menu
        # https://fedoraproject.org/wiki/Changes/HiddenGrubMenu
	msg "Enable Fedora's HiddenGrubMenu"
        patch -Np1 -i ${srcdir}/479dbb9.patch
        patch -Np1 -i ${srcdir}/9c8f258.patch
        patch -Np1 -i ${srcdir}/894d3e2.patch
        patch -Np1 -i ${srcdir}/fd6c8f6.patch
        patch -Np1 -i ${srcdir}/6e9e312.patch
        patch -Np1 -i ${srcdir}/88dc875.patch
        patch -Np1 -i ${srcdir}/1424154.patch
        patch -Np1 -i ${srcdir}/1a52178.patch
        patch -Np1 -i ${srcdir}/ca163ee.patch
        patch -Np1 -i ${srcdir}/96a3829.patch
        patch -Np1 -i ${srcdir}/66f3c60.patch
        patch -Np1 -i ${srcdir}/74fe525.patch
        patch -Np1 -i ${srcdir}/150164a.patch
        # delete line due man h2m
        sed -i -e '1369d' "${srcdir}/grub-${_pkgver}/Makefile.util.def"

	#Apply and Delete the last n line of ubuntu patch (maybe_quiet) because manjaro not provide a rescue mode
	#See here https://answers.launchpad.net/ubuntu/+source/grub2/+question/353038 ( thank to Colin Watson )
	sed -i -e '339,$d' ${srcdir}/maybe_quiet.patch
	patch -p1 -i ${srcdir}/maybe_quiet.patch
        sed -i -e '/message=/d' util/grub.d/10_linux.in
        sed -i -e "/echo	'/d" util/grub.d/10_linux.in
}

_build_grub-common_and_bios() {
	
	msg "Set ARCH dependent variables for bios build"
	if [[ "${CARCH}" == 'x86_64' ]]; then
		_EFIEMU="--enable-efiemu"
	else
		_EFIEMU="--disable-efiemu"
	fi
	
	msg "Copy the source for building the bios part"
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-bios"
	cd "${srcdir}/grub-${_pkgver}-bios/"
	
	msg "Add the grub-extra sources for bios build"
	install -d "${srcdir}/grub-${_pkgver}-bios/grub-extras"
	#cp -r "${srcdir}/grub-extras-${_pkgver}/915resolution" "${srcdir}/grub-${_pkgver}-bios/grub-extras/915resolution"
	export GRUB_CONTRIB="${srcdir}/grub-${_pkgver}-bios/grub-extras/"
	
	msg "Unset all compiler FLAGS for bios build"
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS
	
	cd "${srcdir}/grub-${_pkgver}-bios/"
	
	msg "Run autogen.sh for bios build"
	./autogen.sh
	echo
	
	msg "Run ./configure for bios build"
	./configure \
		--with-platform="pc" \
		--target="i386" \
		"${_EFIEMU}" \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix="/usr" \
		--bindir="/usr/bin" \
		--sbindir="/usr/bin" \
		--mandir="/usr/share/man" \
		--infodir="/usr/share/info" \
		--datarootdir="/usr/share" \
		--sysconfdir="/etc" \
	 	--program-prefix="" \
		--with-bootdir="/boot" \
		--with-grubdir="grub" \
		--disable-silent-rules \
		--disable-werror \
		--enable-quiet-boot \
		--enable-quick-boot
	echo
	
	msg "Run make for bios build"
	make
	echo
}

_build_grub-efi() {
	
	msg "Copy the source for building the ${_EFI_ARCH} efi part"
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}"
	cd "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}/"
	
	msg "Unset all compiler FLAGS for ${_EFI_ARCH} efi build"
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS
	
	cd "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}/"
	
	msg "Run autogen.sh for ${_EFI_ARCH} efi build"
	./autogen.sh
	echo
	
	msg "Run ./configure for ${_EFI_ARCH} efi build"
	./configure \
		--with-platform="efi" \
		--target="${_EFI_ARCH}" \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix="/usr" \
		--bindir="/usr/bin" \
		--sbindir="/usr/bin" \
		--mandir="/usr/share/man" \
		--infodir="/usr/share/info" \
		--datarootdir="/usr/share" \
		--sysconfdir="/etc" \
		--program-prefix="" \
		--with-bootdir="/boot" \
		--with-grubdir="grub" \
		--disable-silent-rules \
		--disable-werror \
		--enable-quiet-boot \
		--enable-quick-boot
	echo
	
	msg "Run make for ${_EFI_ARCH} efi build"
	make
	echo
	
}

build() {
	
	cd "${srcdir}/grub-${_pkgver}/"
	
	msg "Build grub bios stuff"
	_build_grub-common_and_bios
	echo
	
	msg "Build grub ${_EFI_ARCH} efi stuff"
	_build_grub-efi
	echo
	
}

_package_grub-common_and_bios() {
	
	cd "${srcdir}/grub-${_pkgver}-bios/"
	
	msg "Run make install for bios build"
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install
	echo
	
	msg "Remove gdb debugging related files for bios build"
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/{kernel.exec,gdb_grub,gmodule.pl} || true
	
	msg "Install /etc/default/grub (used by grub-mkconfig)"
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"
	
	msg "Install grub.cfg for backup array"
	install -D -m0644 "${srcdir}/grub.cfg" "${pkgdir}/boot/grub/grub.cfg"

	msg "Install update-grub"
	install -Dm755 "${srcdir}/update-grub" "${pkgdir}/usr/bin/update-grub"

	msg "Install grub background"
	install -Dm644 "${srcdir}/background.png" "${pkgdir}/usr/share/grub/background.png"	
}

_package_grub-efi() {
	
	cd "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}/"
	
	msg "Run make install for ${_EFI_ARCH} efi build"
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install
	echo
	
	msg "Remove gdb debugging related files for ${_EFI_ARCH} efi build"
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/{kernel.exec,gdb_grub,gmodule.pl} || true
	
}

package() {
	
	msg "Package grub ${_EFI_ARCH} efi stuff"
	_package_grub-efi
	
	msg "Package grub bios stuff"
	_package_grub-common_and_bios

	install -D -m644 "${srcdir}/${_pkgname}.hook" "${pkgdir}/usr/share/libalpm/hooks/99-${_pkgname}.hook"
	
}
